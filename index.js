Hooks.on("init", () => {
    game.settings.register("pf2e-equipment-checker", "apb-items", {
        name: "Automatic Bonus Progression Benefits Items",
        hint: "Count the benefits from the Automatic Bonus Progression variant rule an item. Eg. Count Attack Potency +1 as a 2nd level item.",
        scope: "world",
        type: Boolean,
        default: false,
        config: true,
    });
    game.settings.register("pf2e-equipment-checker", "caster-apb-adjust", {
        name: "Casters Ignore Martial APB Benefits",
        hint: 'Casters ignore the martial benefits from the Automatic Bonus Progression variant. Does nothing if "Automatic Bonus Progression Benefits Items" is disabled.',
        scope: "world",
        type: Boolean,
        default: false,
        config: true,
    });
});

Hooks.on("renderActorSheet", (sheet, $html) => {
    const $button = $(
        '<a class="item-edit" style="float: right; position: relative; left: 3px;"><i class="fas fa-question-circle"></i></a>'
    ).on("click", () => {
        globalThis.EC = {
            Actor: sheet.actor.data,
            HTML: $html,
        };
        console.log(sheet);
        new Application({
            width: 450,
            height: "fit-content",
            popOut: true,
            minimizable: true,
            resizable: false,
            id: "pf2e-equipment-checker-" + sheet.actor.data.name.replace(/ /g, ""),
            template: "modules/pf2e-equipment-checker/application.html",
            title: `Equipment Checker (${sheet.actor.data.name})`,
        }).render(true);
    });
    $html.find("h3:contains('Total Wealth')").append($button);
});
