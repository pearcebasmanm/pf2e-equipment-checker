# PF2e Equipment Checker

An easy way to check your equipment balance in the Pathfinder 2e system.
Click the question mark button next to Total Wealth in the Inventory tab to access it.

### Manual Install

In Foundry setup, click on the Install Module button and put the following path in the Manifest URL.

`https://gitlab.com/pearcebasmanm/pf2e-equipment-checker/-/raw/main/module.json`
